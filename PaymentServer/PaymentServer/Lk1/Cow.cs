﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentServer.Lk1
{
    public class Cow : Animal
    {
        public Cow(string name, string gender, int age, float weight, string color) :
            base(name, gender, age, weight, color)
        {
        }

        public override string GetSomething()
        {
            return "milk";
        }

        public override void Speak()
        {
            Console.WriteLine("Moo");
        }
    }
}
