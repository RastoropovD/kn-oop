﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentServer.Lk1
{
    public class Cat : Animal
    {

        public Cat(string name, string gender, int age, float weight, string color) : 
            base(name, gender, age, weight, color)
        {
        }

        public override void Speak()
        {
            Console.WriteLine("Meow");
        }

        public override string GetSomething()
        {
            Console.WriteLine("Take something");
            return "fur";
        }

        

    }
}
