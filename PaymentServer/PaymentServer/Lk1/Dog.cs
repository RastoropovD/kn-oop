﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentServer.Lk1
{
    public class Dog : IAnimal
    {
        public string Name { get; private set; }

        public string Gender { get; private set; }

        public int Age { get; private set; }

        public float Weight { get; private set; }

        public string Color { get; private set; }

        public Dog(string name, string gender, int age, float weight, string color)
        {
            Name = name; Gender = gender; Age = age; Weight = weight; Color = color;
        }

        public void Eat(string food)
        {
            Console.WriteLine("I ate {0}", food);
            Weight = Weight + 0.1f;
        }

        public string GetSomething()
        {
            return "fur";
        }

        public void Speak()
        {
            Console.WriteLine("Woof!");
        }
    }
}
