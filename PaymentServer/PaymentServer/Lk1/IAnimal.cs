﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentServer.Lk1
{
    interface IAnimal
    {

        void Speak();

        string GetSomething();

        void Eat(string food);
    }
}
