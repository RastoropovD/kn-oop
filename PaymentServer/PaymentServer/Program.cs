﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymentServer.Lk1;
using BL.Service.PaymentSystems;
using BL.ViewModel;
using DAL.Domain;

namespace PaymentServer
{
    class Program
    {

        static void Main(string[] args)
        {
            #region lecture 1
            /*
            Cat pushistik = new Cat("Pushistik", "male", 3, 5.5f, "gray");
            Cat murka = new Cat("Murka", "female", 1, 3.5f, "white");


            pushistik.Speak();
            murka.Speak();

            pushistik.Eat("Bread");
            murka.Eat("Meat");

            Console.WriteLine(pushistik.GetSomething());


            Cow milka = new Cow("Milka", "female", 2, 10f, "white");

            milka.Speak();
            milka.Eat("Grass");
            Console.WriteLine("{0} gives {1}", milka.Name, milka.GetSomething());
            */
            #endregion
            Console.WriteLine("Start...");

            PaymentSystemContainer container = PaymentSystemContainer.GetInstance();

            //list of payways 
            Dictionary<string, PaymentSystemType> payways = new Dictionary<string, PaymentSystemType>
            {
                { "pw1", PaymentSystemType.SYSTEM1 },
                { "pw2", PaymentSystemType.SYSTEM2 },
                { "pw3", PaymentSystemType.SYSTEM3 }
            };

            //some payin request
            PayInRequest request1 = new PayInRequest()
            {
                Payway = "pw1",
                Amount = 100,
                UserName = "user1",
                Currency = CurrencyType.UAH,
                CardMask = "123456*****7895"
            };

            PayInRequest request2 = new PayInRequest()
            {
                Payway = "pw2",
                Amount = 100,
                UserName = "user2",
                Currency = CurrencyType.USD,
                CardMask = "123456*****7895"
            };

            PayInRequest request3 = new PayInRequest()
            {
                Payway = "pw3",
                Amount = 100
            };

            PayIn result1 = container.GetByPaymentSystemType(payways[request1.Payway]).HandlePayIn(request1);
            PayIn result2 = container.GetByPaymentSystemType(payways[request2.Payway]).HandlePayIn(request2);
            //   PayIn result3 = container.GetByPaymentSystemType(payways[request3.Payway]).HandlePayIn(request3);


            Console.WriteLine("Finished");

            Console.Read();
        }

    }
}


