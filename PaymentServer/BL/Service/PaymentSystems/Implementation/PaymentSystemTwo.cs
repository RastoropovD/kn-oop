﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.ViewModel;
using DAL.Domain;
using BL.Service;

namespace BL.Service.PaymentSystems.Implementation
{
    public class PaymentSystemTwo : IPaymentSystemAdapter
    {
        private PayInService service;

        public PaymentSystemType GetPaymentSystem()
        {
            return PaymentSystemType.SYSTEM2;
        }

        public PaymentSystemTwo()
        {
            service = new PayInService();
            PaymentSystemContainer container = PaymentSystemContainer.GetInstance();
            container.RegisterPaymentSystem(this);
        }

        public PayIn HandlePayIn(PayInRequest request)
        {
            //some logic with handling payin request
            return service.Save(request);
        }

        public PayOut HandlePayOut(PayOutRequest request)
        {
            //some logic with handling payout request
            throw new NotImplementedException();
        }
    }
}
