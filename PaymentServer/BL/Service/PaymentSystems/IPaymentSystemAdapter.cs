﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using BL.ViewModel;

namespace BL.Service.PaymentSystems
{
    public interface IPaymentSystemAdapter
    {
        PaymentSystemType GetPaymentSystem();

        PayIn HandlePayIn(PayInRequest request);

        PayOut HandlePayOut(PayOutRequest request);
    }
}
