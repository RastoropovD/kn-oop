﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BL.Service.PaymentSystems
{
    public class PaymentSystemContainer
    {
        private Dictionary<PaymentSystemType, IPaymentSystemAdapter> container;

        private static PaymentSystemContainer instance;
        private PaymentSystemContainer() { }

        public static PaymentSystemContainer GetInstance()
        {
            if(instance == null)
            {
                instance = new PaymentSystemContainer();
                instance.container = new Dictionary<PaymentSystemType, IPaymentSystemAdapter>();
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(s => s.GetTypes())
                    .Where(p => typeof(IPaymentSystemAdapter).IsAssignableFrom(p))
                    .Where(p => !p.IsInterface)
                    .Select(p => Activator.CreateInstance(p)).ToList();
            }
            return instance;
        }


        public void RegisterPaymentSystem(IPaymentSystemAdapter paymentSystemAdapter)
        {
            container.Add(paymentSystemAdapter.GetPaymentSystem(), paymentSystemAdapter);
        }


        public IPaymentSystemAdapter GetByPaymentSystemType(PaymentSystemType systemType)
        {
            if(container.ContainsKey(systemType))
            {
                return container[systemType];
            }
            throw new NotImplementedException("Adapter for system " + systemType + " not found");
        }


    }
}
