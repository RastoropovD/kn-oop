﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using DAL.Repository;
using DAL.FileStorage;
using DAL.Database;
using BL.ViewModel;

namespace BL.Service
{
    public class PayInService
    {
        private IRepository<PayIn> repository;

        public PayInService()
        {
            //ToDo: getting instance of repository implementation
            //repository = new PayInDbRepository();
            repository = new PayInFileRepository();
        }

        public PayIn Save(PayInRequest request)
        {
            PayIn entity = new PayIn()
            {
                PaymentId = 0,
                UserName = request.UserName,
                Amount = request.Amount,
                Currency = request.Currency,
                IsNotified = false,
                CardMask = request.CardMask
            };
            //some logic...
            return repository.Update(entity);
        }


        public void Delete(int id)
        {
            //some logic
            repository.Delete(id);
        }
        
        public List<PayIn> FindAll()
        {
            //some logic
            return repository.FindAll().ToList();
        }

        public PayIn FindOne(int id)
        {
            //some logic
            return repository.FindOne(id);
        }

    }
}
