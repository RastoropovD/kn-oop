﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;

namespace BL.ViewModel
{
    public class PayInRequest
    {
        public string Payway { get; set; }

        public decimal Amount { get; set; }

        public string UserName { get; set; }

        public CurrencyType Currency { get; set; }

        public string CardMask { get; set; }
    }
}
