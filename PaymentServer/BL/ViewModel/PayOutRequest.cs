﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.ViewModel
{
    public class PayOutRequest
    {
        public string Payway { get; set; }

        public decimal Amount { get; set; }

        //some other properties
    }
}
