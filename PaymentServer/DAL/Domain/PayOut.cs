﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Domain
{
    [Serializable]
    public class PayOut
    {
        public int PaymentId { get; set; }

        public string UserName { get; set; }

        public decimal Amount { get; set; }

        public CurrencyType Currency { get; set; }

        public bool IsNotified { get; set; }

        public decimal Comission { get; set; }
    }
}
