﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace DAL.Domain
{
    [Serializable]
    public class PayIn
    {
        public int PaymentId;
        
        public string UserName;
        
        public decimal Amount;
        
        public CurrencyType Currency;
        
        public bool IsNotified;
       
        public string CardMask;
    }
}
