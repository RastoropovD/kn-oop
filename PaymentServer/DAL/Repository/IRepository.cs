﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public interface IRepository<T>
    {
        IEnumerable<T> FindAll();

        T FindOne(int id);

        T Update(T entity);

        T Delete(int id);
         
    }
}
