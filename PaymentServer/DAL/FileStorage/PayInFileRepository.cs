﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using DAL.Repository;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;

namespace DAL.FileStorage
{
    public class PayInFileRepository : IRepository<PayIn>
    {
        //private readonly XmlSerializer _serializer;
        //private readonly BinaryFormatter _serializer;
       private readonly DataContractJsonSerializer _serializer;

        public PayInFileRepository()
        {
            //_serializer = new XmlSerializer(typeof(PayIn));
            //_serializer = new BinaryFormatter();
            _serializer = new DataContractJsonSerializer(typeof(PayIn));
        }

        public PayIn Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PayIn> FindAll()
        {
            throw new NotImplementedException();
        }

        public PayIn FindOne(int id)
        {
            //using(FileStream fs = new FileStream($"/payin-{id}.xml", FileMode.OpenOrCreate))
            //using (FileStream fs = new FileStream($"/payin-{id}.dat", FileMode.OpenOrCreate))
            using (FileStream fs = new FileStream($"/payin-{id}.json", FileMode.OpenOrCreate))
            {
             //   PayIn entity = (PayIn)_serializer.Deserialize(fs);
                PayIn entity = (PayIn)_serializer.ReadObject(fs);
                return entity;
            }
        }

        public PayIn Update(PayIn entity)
        {
            //using(FileStream fs = new FileStream($"../payin-{entity.PaymentId}.xml", FileMode.OpenOrCreate))
            //using (FileStream fs = new FileStream($"../payin-{entity.PaymentId}.dat", FileMode.OpenOrCreate))
            using (FileStream fs = new FileStream($"../payin-{entity.PaymentId}.json", FileMode.OpenOrCreate))
            {
            //    _serializer.Serialize(fs, entity);
              _serializer.WriteObject(fs, entity);
                return entity;
            }
        }
    }
}
