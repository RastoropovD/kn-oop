﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CarStation
{
    class Program
    {
        static void Main(string[] args)
        {

            #region array
            string[] name = new string[10];

            for(int i = 0; i < name.Length; i++)
            {
                name[i] = $"item{i}";
            }
            #endregion

            #region list
            List<string> lst = new List<string>();
            List<string> lst2 = new List<string>()
            {
                "item-1", "item-2", "item-3"
            };

            for (int i = 0; i < 15; i++)
            {
                lst.Add($"item{i}");
            }

            foreach(var item in name)
            {
                Console.WriteLine(item);
            }

            lst.AddRange(lst2);
            lst.AddRange(name);
            Console.WriteLine($"{lst.Count()}");
            #endregion

            #region dictionary
            Console.WriteLine("\n\n DICTIONARY \n");
            Dictionary<string, object> dict = new Dictionary<string, object>();

            dict.Add("first", "some value");
            dict.Add("second", 10);
            dict.Add("third", new string[5] {"1", "2", "3", "4", "5"});
            
            foreach(var item in dict)
            {
                int tmp;
                if(int.TryParse(item.Value.ToString(), out tmp))
                {
                    Console.WriteLine(tmp);
                }
            }
            #endregion

            #region queue
            Console.WriteLine("\n\n QUEUE \n");

            Queue<string> myQueue = new Queue<string>();

            myQueue.Enqueue("first");
            myQueue.Enqueue("second");
            myQueue.Enqueue("third");

            foreach(var item in myQueue)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine(myQueue.Count());
            string queueItem = myQueue.Dequeue();
            Console.WriteLine($"got from queue {queueItem}");
            Console.WriteLine(myQueue.Count());
            #endregion

            #region stack
            Console.WriteLine("\n\n STACK \n");

            Stack<string> myStack = new Stack<string>();

            myStack.Push("first");
            myStack.Push("second");
            myStack.Push("third");

            Console.WriteLine($"{myStack.Count()}");
            Console.WriteLine($"{myStack.Pop()}");
            Console.WriteLine($"{myStack.Count()}");

            #endregion

            var parkingSlot1 = new ParkingSlotModel(1, 10);


            Console.Read();
        }

        class CarModel
        {
            private int _maxCapacity;

            public List<string> Passangers { get; private set; }



            void AddPassanger(string passanger)
            {
                if(Passangers.Count() < _maxCapacity)
                    Passangers.Add(passanger);
            }

        }


        class ParkingSlotModel
        {
            public int SlotType { get; private set; }

            public decimal Price { get; private set; }

            public bool IsFree { get; set; }

            public ParkingSlotModel(int SlotType, decimal Price)
            {
                this.SlotType = SlotType;
                this.Price = Price;
            }
        }
    }
}
